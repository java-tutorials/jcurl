package jCURL;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.proximus.bdp.restapi.Jcurl;

public class oozieTest {
	private String URI = "http://sandbox.hortonworks.com:11000/oozie/v1/jobs";

	@Test
	public final void valStartJob() throws IOException {
		String args[] = new String[7];
		args[0] = "-X";
		args[1] = "POST";
		args[2] = "-d";
		args[3] = "@src/test/resources/oozie-config.xml";
		args[4] = "-H";
		args[5] = "\"Content-Type: application/xml;charset=utf-8\"";
		args[6] = "http://sandbox.hortonworks.com:11000/oozie/v1/jobs?action=start";
		for(String arg : args)
			System.out.print(arg + " ");
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 201 Created"));
	}
	
	@Test
	public final void valListJobs() throws IOException {
		String args[] = new String[3];
		args[0] = "-X";
		args[1] = "GET";
		args[2] = URI;
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 200 OK"));
	}

}
