package jCURL;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.proximus.bdp.restapi.Jcurl;

public class JSONTests {
	
	private String URI = "http://jsonplaceholder.typicode.com/posts";
	
	private static final String MESSAGE = "{\"userId\": 1,\"id\": 1,\"title\": \"My Title\",\"body\": \"My Body\"}";

	@Test
	public final void  valGETsimple() throws IOException {
		String args[] = new String[1];
		args[0] = URI + "/1";
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 200 OK"));
	}

	@Test
	public final void  valPOSTfromCLI() throws IOException {
		String args[] = new String[5];
		args[0] = "-d";
		args[1] = MESSAGE;
		args[2] = "-X";
		args[3] = "POST";
		args[4] = URI;
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 201 Created"));
	}
	
	@Test
	public final void  valPOSTfromFile() throws IOException {
		String args[] = new String[5];
		args[0] = "-d";
		args[1] = "@src/test/resources/message.json";
		args[2] = "-X";
		args[3] = "POST";
		args[4] = URI;
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 201 Created"));
	}
	
	@Test
	public final void  valPOSTJSONfromFile() throws IOException {
		String args[] = new String[8];
		args[0] = "-d";
		args[1] = "@src/test/resources/message.json";
		args[2] = "-X";
		args[3] = "POST";
		args[4] = "-H";
		args[5] = "\"Content-Type:application/json;charset=utf-8\"";
		args[6] = "-i";
		args[7] = URI;
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 201 Created"));
	}
	
	@Test
	public final void  valPOSTXMLfromFile() throws IOException {
		String args[] = new String[7];
		args[0] = "-d";
		args[1] = "@src/test/resources/message.json";
		args[2] = "-X";
		args[3] = "POST";
		args[4] = "-H";
		args[5] = "\"Content-Type:application/xml;charset=utf-8\"";
		args[6] = URI;
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 201 Created"));
	}

}
