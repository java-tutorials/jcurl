package jCURL;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.proximus.bdp.restapi.Jcurl;

public class HDFSTests {

	@Test
	public final void valGETsimple() throws IOException {
		String args[] = new String[1];
		args[0] = "http://sandbox.hortonworks.com:50070/webhdfs/v1/?op=GETFILESTATUS";
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 200 OK"));
	}
	
	@Test
	public final void valGETparams() throws IOException {
		String args[] = new String[3];
		args[0] = "-X";
		args[1] = "GET";
		args[2] = "http://sandbox.hortonworks.com:50070/webhdfs/v1/?op=GETFILESTATUS";
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 200 OK"));
	}
	
	@Test
	public final void valPUTdir() throws IOException {
		String args[] = new String[3];
		args[0] = "-X";
		args[1] = "PUT";
		args[2] = "http://sandbox.hortonworks.com:50070/webhdfs/v1/tmp/sample-dir?op=MKDIRS";
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 200 OK"));
	}
	
	@Test
	public final void valPUTfile() throws IOException {
		String args[] = new String[5];
		args[0] = "-X";
		args[1] = "PUT";
		args[2] = "-T";
		args[3] = "src/test/resources/sample-text";
		args[4] = "http://sandbox.hortonworks.com:50070/webhdfs/v1/tmp/sample-text?op=CREATE&overwrite=true";
	    assertTrue(new Jcurl().run(args).startsWith("http/1.1 201 Created"));
	}
	
	@Test
	public final void valDELETEfile() throws IOException {
		String args[] = new String[3];
		args[0] = "-X";
		args[1] = "DELETE";
		args[2] = "http://sandbox.hortonworks.com:50070/webhdfs/v1/tmp/sample-text?op=DELETE";
		assertTrue(new Jcurl().run(args).startsWith("http/1.1 200 OK"));
	}
}
