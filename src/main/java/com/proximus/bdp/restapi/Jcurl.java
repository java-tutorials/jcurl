package com.proximus.bdp.restapi;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Jcurl {
	private final String USAGE = 
			"Usage: jcurl [options...] <url>\n" 
					+ "Options:\n"
					+ " -i            Include protocol headers in the output\n"
					+ " -s            Include response status in the output\n"
					+ " -X COMMAND    Specify request COMMAND to use (GET, POST, PUT, DELETE)\n"
					+ " -d DATA       HTTP POST data, '@FILE' allowed\n"
					+ " -T FILE       Upload FILE to destination\n"
					+ " -H LINE       Pass custom header LINE to server.";

	private final int GET    = 0;
	private final int PUT    = 1;
	private final int POST   = 2;
	private final int DELETE = 3;
	private Options options = new Options();	

	private class Options {
		int method = GET;
		boolean displayHeader = false;
		boolean displayStatus = true;
		boolean putFile = false;
		String body = "";
		String file = "";
		String mediaType = "application/json;charset=utf-8";
		boolean setHeader = false;
		Map<String, String> headers = new HashMap<>();
	}

	public String run(String[] args) throws IOException  {
		OkHttpClient client = new OkHttpClient();
		Response response;

		if(args.length == 0) {
			return(USAGE);
		}

		else if(args.length == 1) {
			response = getMethod(client, args[0]);
			return(getResponseMessage(response));
		}

		else {
			// Parsing command line arguments for invoking specified method
			processArgs(args);
			String URI = args[args.length-1];

			switch(options.method) {
			case(GET):
				response = getMethod(client, URI);
			break;
			case(POST):
				if(options.body.charAt(0) == '@')
					response = postMethodFromFile(client, URI);
				else
					response = postMethod(client, URI);
			break;
			case(PUT):
				if(options.putFile)
					response = putFile(client, URI);
				else
					response = putMethod(client, URI);
			break;
			case(DELETE):
				response = deleteMethod(client, URI);
			break;
			default:
				return("Unknow Method: " + options.method);
			}

			if (!response.isSuccessful())
				return("Unexpected response:\n" + response);
			else
				return getResponseMessage(response);
		}
	}

	/**
	 * Submits a File Upload Request to the resource specified by the WebService location
	 * @param client the HTTP REST client
	 * @param URI the URI of the Web Service to call
	 * @throws IOException
	 * @return the HTTP response
	 */
	private Response putFile(OkHttpClient client, String URI) throws IOException {
		System.out.println("Posting file " + options.file + " to " + URI);

		// Step 1: Submit a HTTP PUT request without automatically following redirects and without sending the file data.		
		Request request = new Request.Builder()
		.url(URI)
		.header("Transfer-Encoding", "chunked")
		.put(RequestBody.create(MediaType.parse("application/octet-stream"), ""))
		.build();

		Response response = client
				.newBuilder()
				.followRedirects(false)
				.build()
				.newCall(request)
				.execute();

		String redirectedLocation = response.header("Location");

		//Step 2: Submit another HTTP PUT request using the URL in the Location header with the file data to be written
		File file = new File(options.file);

		request = new Request.Builder()
		.url(redirectedLocation)
		.put(RequestBody.create(MediaType.parse("application/octet-stream"), file))
		.build();

		response = client.newCall(request).execute();	
		return(response);
	}

	/**
	 * Submits a DELETE Request to the resource specified by the WebService location
	 * @param client the HTTP REST client
	 * @param URI the URI of the Web Service to call
	 * @throws IOException
	 * @return the HTTP response
	 */
	private Response deleteMethod(OkHttpClient client, String URI) throws IOException {
		final MediaType MEDIA_TYPE
		= MediaType.parse(options.mediaType);

		Builder builder = new Request.Builder()
		.url(URI)
		.delete(RequestBody.create(MEDIA_TYPE, options.body));

		if(options.setHeader)
			for(String name : options.headers.keySet())
				builder.addHeader(name, options.headers.get(name));

		Response response = client.newCall(builder.build()).execute();

		return(response);
	}

	/**
	 * Submits a PUT Request to the resource specified by the WebService location
	 * @param client the HTTP REST client
	 * @param URI the URI of the Web Service to call
	 * @throws IOException
	 * @return the HTTP response
	 */
	private Response putMethod(OkHttpClient client, String URI) throws IOException {
		final MediaType MEDIA_TYPE = MediaType.parse(options.mediaType);

		Builder builder = new Request.Builder()
		.url(URI)
		.put(RequestBody.create(MEDIA_TYPE, options.body));

		if(options.setHeader)
			for(String name : options.headers.keySet())
				builder.addHeader(name, options.headers.get(name));

		Response response = client.newCall(builder.build()).execute();

		return(response);
	}

	/**
	 * Submits a POST Request to the resource specified by the WebService location.
	 * The body of the request is specified in the command line with -d option.
	 * @param client the HTTP REST client
	 * @param URI the URI of the Web Service to call
	 * @throws IOException
	 * @return the HTTP response
	 */
	private Response postMethod(OkHttpClient client, String URI) throws IOException {
		final MediaType MEDIA_TYPE = MediaType.parse(options.mediaType);

		Builder builder = new Request.Builder()
		.url(URI)
		.post(RequestBody.create(MEDIA_TYPE, options.body));

		if(options.setHeader)
			for(String name : options.headers.keySet())
				builder.addHeader(name, options.headers.get(name));

		Response response = client.newCall(builder.build()).execute();
		
		return(response);
	}

	/**
	 * Submits a POST Request to the resource specified by the WebService location.
	 * The body of the request is specified in a file using -d @filename option.
	 * @param client the HTTP REST client
	 * @param URI the URI of the Web Service to call
	 * @throws IOException
	 * @return the HTTP response
	 */
	private Response postMethodFromFile(OkHttpClient client, String URI) throws IOException {
		final MediaType MEDIA_TYPE = MediaType.parse(options.mediaType);

		// Ignore the first @ character for filename
		File file = new File(options.body.substring(1));

		Builder builder = new Request.Builder()
			.url(URI)
			.post(RequestBody.create(MEDIA_TYPE, file));
		
		if(options.setHeader)
			for(String name : options.headers.keySet())
				builder.addHeader(name, options.headers.get(name));

		Response response = client.newCall(builder.build()).execute();
		return(response);
	}

	/**
	 * Submits a GET Request to the resource specified by the WebService location
	 * @param client the HTTP REST client
	 * @param URI the URI of the Web Service to call
	 * @throws IOException
	 * @return the HTTP response
	 */
	private Response getMethod(OkHttpClient client, String URI) throws IOException {
		Builder builder = new Request.Builder().url(URI);
		
		// Add request headers
		if(options.setHeader)
			for(String name : options.headers.keySet())
				builder.addHeader(name, options.headers.get(name));

		Response response = client.newCall(builder.build()).execute();
		return(response);
	}

	/**
	 * Validates and sets the various arguments from the command line
	 * @param args
	 */
	private void processArgs(String args[]) {
		for(int i = 0; i < args.length -1; i++) {
			switch(args[i]) {
			case "-i": // Display header response
				options.displayHeader = true;
				break;

			case "-s": // Display response status
				options.displayStatus = true;
				break;

			case "-X": // Sets HTTP method (GET, PUT, POST, DELETE)
				setMethod(args[++i]);
				break;

			case "-d": // Specifies data body to submit
				options.body = args[++i];
				break;

			case "-T": // Specifies file body to upload			
				options.file = args[++i];
				options.putFile = true;
				break;

			case "-H": // Specifies the media type
				options.setHeader  = true;
				processHeader(args[++i]);
				break;

			case " ":
				break;

			default:
				System.out.println("Incorrect invoquation");
				System.exit(-1);
			}
		}
	}

	/**
	 * Sets the request Headers with the specified String parameter
	 * If the header specifies the payload content type, sets OkHTTP media type accordingly
	 * @param headerString the String containing the header specification
	 */
	private void processHeader(String headerString) {
		int separatorIndex = headerString.indexOf(':');
		int startIndex = 0;
		int endIndex = headerString.length();
		
		if(separatorIndex == -1)
			System.out.println("Not a valid header: " + headerString);
			
		else {
			// Check if header is surrounded by hyphens
			if(headerString.charAt(0) == '"') {
				startIndex++;
				endIndex--;
			}
				
			options.headers.put(headerString.substring(startIndex, separatorIndex), 
					headerString.substring(separatorIndex+1, endIndex));
			
			// Check if header specifies the payload content type and sets OkHTTP media type accordingly
			if(headerString.startsWith("Content-Type"))
				options.mediaType = headerString;
		}

	}

	/**
	 * Validates and sets the HTTP method option to use in the request (-X option)
	 * @param method the String containing the HTTP method
	 */
	private void setMethod(String method) {
		switch(method) {
		case "GET":
			options.method = GET;
			break;
		case "PUT":
			options.method = PUT;
			break;
		case "POST":
			options.method = POST;
			break;
		case "DELETE":
			options.method = DELETE;
			break;
		default:
			System.out.println("Unknown method -X " + method);
			System.exit(-1);
		}
	}

	/**
	 * Prints out on standard output the received HTTP response based on the options defined in the command line.
	 * @param response the HTTP response
	 * @throws IOException
	 */
	private String getResponseMessage(Response response) throws IOException {
		StringBuilder responseMsg =  new StringBuilder();

		if(options.displayStatus)
			responseMsg.append(response.protocol().toString())
			.append(" ")
			.append(response.code())
			.append(" ")
			.append(response.message())
			.append("\n");;

		if(options.displayHeader)
			responseMsg.append(getResponseHeaders(response.headers()));

		responseMsg.append(response.body().string());

		return responseMsg.toString();
	}

	/**
	 * Prints out on standard output the received HTTP Headers
	 * @param responseHeaders the received HTTP Headers
	 */
	private String getResponseHeaders(Headers responseHeaders) {
		StringBuilder responseHeader =  new StringBuilder();

		for (int i = 0; i < responseHeaders.size(); i++) {
			responseHeader.append(responseHeaders.name(i))
			.append(": ")
			.append(responseHeaders.value(i))
			.append("\n");
		}

		return responseHeader.toString();
	}
}