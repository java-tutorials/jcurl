package com.proximus.bdp.restapi;

import java.io.IOException;

public class JcurlCLI {
	
	public static void main(String[] args) throws IOException {
		System.out.println(new Jcurl().run(args));
	}
}